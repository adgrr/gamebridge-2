import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore'
import { Router } from '@angular/router'
import { AlertController } from '@ionic/angular'
import { User } from '../../interfaces/User'


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(
      private afAuth: AngularFireAuth,
      private afs: AngularFirestore,
      private router: Router,
      private alertController: AlertController,
  ) { }

  // Start the Firebase register process
  async emailSignup({email, password}): Promise<any> {
    const credential = await this.afAuth.createUserWithEmailAndPassword(
        email,
        password
    )
    return this.updateUserData(credential.user)
  }

  signIn({email, password}) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
  }

  resetPw(email) {
    return this.afAuth.sendPasswordResetEmail(email)
  }

  // Sets user data to firestore on login
  private updateUserData(user: User, image = null, username = null): Promise<any> {

    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`)

    const data = {
      uid: user.uid,
      email: user.email,
      photoURL: image,
      username
    }

    return userRef.set(data, { merge: true })
  }

  /**
   * Get the current user logged in
   */
  getCurrentUser(): Promise<User>{
    return this.afAuth.currentUser.then((user: User | null | undefined) => {
      return user
    })
  }

  /**
   * Return a user by uid
   */
  getUserByUid(uid: string): Promise<any> {
    return this.afs.collection('users', ref => ref.where('uid', '==', uid)).get().toPromise()
        .then((snapshot) => {
          return snapshot.docs.map((doc) => {
            return doc.data()
          })
        })
  }

  /**
   * Delete the current user account
   */
  async deleteCurrentUser() {
    const user = await this.afAuth.currentUser

    user.delete().then(() => {
      this.router.navigateByUrl('/login')
    }).catch(async () => {
      const alert = await this.alertController.create({
        header: 'Erreur lors de la suppression',
        message: 'Pour des raisons de sécurité, vous devez vous déconnecter et reconnecter afin d\'effectuer cette action',
        buttons: ['OK'],
      })

      await alert.present()
    })
  }
}
