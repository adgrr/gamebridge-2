import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Game } from '../../../interfaces/Game'
import { HttpClient } from '@angular/common/http'
import { ScreensizeService } from '../../../services/screensize/screensize.service'
import { LoadingController } from '@ionic/angular'
import { Plugins } from '@capacitor/core'
const  { Storage } = Plugins


@Component({
  selector: 'app-session-game',
  templateUrl: './session-game.page.html',
  styleUrls: ['./session-game.page.scss'],
})


export class SessionGamePage implements OnInit {

  allGames: Game[] = null
  searchInput = ''
  isDesktop: boolean = null

  constructor(
      private router: Router,
      private http: HttpClient,
      private screensizeService: ScreensizeService,
      private loadingController: LoadingController,
  ) {
    this.screensizeService.isDesktopView().subscribe(isDesktop => {
      if (this.isDesktop && !this.isDesktop){
        window.location.reload()
      }
      this.isDesktop = isDesktop
    })
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    // Show loading
    const loading = await this.loadingController.create()
    await loading.present()

    this.callAllGames().then(() => {
      loading.dismiss()
    })
  }

  private callAllGames(): Promise<Game[]>{
    return new Promise (resolve => {
      this.http
          .get<Game[]>('https://us-central1-gamebridge-app.cloudfunctions.net/getAllGames ', {
            responseType: 'json', headers: {'Access-Control-Allow-Origin': '*'}
          })
          .subscribe((data: any) => {
            this.allGames = data.json
            resolve(this.allGames)
          })
    })
  }

  async saveAndNextStep(game: Game): Promise<void> {
    // Constructing the session to be send at the end of the process
    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: game.name,
        gameId: game.id,
        mode: null,
        goal: null,
        platform: null,
        date: null,
        description: null,
        createdAt: null,
        expiredAt: null,
        createdBy: null,
        listPlayers: []
      })
    })

    // Useful for the following steps
    await Storage.set({
      key: 'tmpGame',
      value: JSON.stringify({game})
    })

    await this.router.navigateByUrl('tabs/session/mode')
  }

  cancelSession() {
    this.router.navigateByUrl('tabs/homepage').then(r => console.log(r))
  }

  searchFor(event) {
    this.searchInput = event.detail.value
  }
}
