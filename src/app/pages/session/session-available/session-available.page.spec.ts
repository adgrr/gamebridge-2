import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { SessionAvailablePage } from './session-available.page'

describe('SessionAvailablePage', () => {
  let component: SessionAvailablePage
  let fixture: ComponentFixture<SessionAvailablePage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionAvailablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(SessionAvailablePage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
