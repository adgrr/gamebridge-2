import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionAvailablePageRoutingModule } from './session-available-routing.module'

import { SessionAvailablePage } from './session-available.page'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SessionAvailablePageRoutingModule
  ],
  declarations: [SessionAvailablePage]
})
export class SessionAvailablePageModule {}
