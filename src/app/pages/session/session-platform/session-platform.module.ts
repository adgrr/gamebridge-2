import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionPlatformPageRoutingModule } from './session-platform-routing.module'

import { SessionPlatformPage } from './session-platform.page'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SessionPlatformPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [SessionPlatformPage]
})
export class SessionPlatformPageModule {}
