import { Component, OnInit } from '@angular/core'
import { Plugins } from '@capacitor/core'
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoadingController } from '@ionic/angular'
const  { Storage } = Plugins

@Component({
  selector: 'app-session-date',
  templateUrl: './session-date.page.html',
  styleUrls: ['./session-date.page.scss'],
})
export class SessionDatePage implements OnInit {

  dateForm: FormGroup
  choices: any[] = []

  constructor(
      private fb: FormBuilder,
      private router: Router,
      private loadingController: LoadingController,
  ) { }


  ngOnInit() {
    this.dateForm = this.fb.group({
      date: [null, [Validators.required]],
      datePicker: [null]
    })
  }

  async ionViewWillEnter() {
    // Show loading
    const loading = await this.loadingController.create()
    await loading.present()

    this.getSessionFromStorage().then(() => {
      loading.dismiss()
    })
  }

  async saveAndNextStep(): Promise<void> {
    // Get previous data in localStorage
    const sessionStorage = await Storage.get({key: 'session'})
    const session = JSON.parse(sessionStorage.value)

    // Values already set
    const gamename = session.game
    const gameId = session.gameId
    const mode = session.mode
    const goal = session.goal
    const platform = session.platform

    // If now selected
    let inputDate = this.dateForm.value.date

    // new Date(Date.now()).toISOString();
    if (inputDate === 'now'){
      inputDate = Date.now()
    } else {
      inputDate = new Date(this.dateForm.value.datePicker).getTime()
    }

    // Set the new data
    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: gamename,
        gameId,
        mode,
        goal,
        platform,
        date: inputDate,
        description: null,
        createdAt: null,
        expiredAt: null,
        createdBy: null,
        listPlayers: []
      })
    })

    // Debugging
    const sessionMemory = await Storage.get({key: 'session'})
    const sessionAfter = JSON.parse(sessionMemory.value)
    console.log(sessionAfter)

    await this.router.navigateByUrl('tabs/session/description')
  }

  private getSessionFromStorage: () => Promise<any[]> = async ()  => {
    return new Promise (async resolve => {
      const sessionStorage = await Storage.get({key: 'session'})
      const session = JSON.parse(sessionStorage.value)

      this.choices.push(session.game, session.mode, session.goal, session.platform)

      resolve(this.choices)
    })
  }
}
