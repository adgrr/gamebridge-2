import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { SessionDescriptionPage } from './session-description.page'

describe('SessionDescriptionPage', () => {
  let component: SessionDescriptionPage
  let fixture: ComponentFixture<SessionDescriptionPage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionDescriptionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(SessionDescriptionPage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
