import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionDescriptionPage } from './session-description.page'

const routes: Routes = [
  {
    path: '',
    component: SessionDescriptionPage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionDescriptionPageRoutingModule {}
