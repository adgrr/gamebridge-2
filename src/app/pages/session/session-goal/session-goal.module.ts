import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionGoalPageRoutingModule } from './session-goal-routing.module'

import { SessionGoalPage } from './session-goal.page'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SessionGoalPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [SessionGoalPage]
})
export class SessionGoalPageModule {}
