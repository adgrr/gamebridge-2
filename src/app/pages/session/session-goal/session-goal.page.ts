import { Component, OnInit } from '@angular/core'
import { Plugins } from '@capacitor/core'
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { HttpClient } from '@angular/common/http'
import { Goal } from '../../../interfaces/Goal'
import { LoadingController } from '@ionic/angular'
const  { Storage } = Plugins

@Component({
  selector: 'app-session-goal',
  templateUrl: './session-goal.page.html',
  styleUrls: ['./session-goal.page.scss'],
})

export class SessionGoalPage implements OnInit {

  allGoals: Goal[] = null
  goalForm: FormGroup
  choices: any[] = []

  constructor(
      private fb: FormBuilder,
      private router: Router,
      private http: HttpClient,
      private loadingController: LoadingController,
  ) { }

  async ngOnInit() {

  }

  async ionViewWillEnter() {
    this.goalForm = this.fb.group({
      goalRadio: [null, [Validators.required]],
    })

    // Show loading
    const loading = await this.loadingController.create()
    await loading.present()

    await Promise.all([this.callAllGoals(), this.getSessionFromStorage()])
        .then(() => { loading.dismiss() })
        .catch(err => console.log(err))
  }


  private callAllGoals(): Promise<Goal[]> {
    return new Promise (resolve => {
      this.http
          .get('https://us-central1-gamebridge-app.cloudfunctions.net/getAllGoals', {
            responseType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
          })
          .subscribe((data: any) => {
            this.allGoals = data.json
            resolve(this.allGoals)
          })
    })

  }

  getSessionFromStorage: () => Promise<any[]> = async ()  => {
    return new Promise (async resolve => {
      const sessionStorage = await Storage.get({key: 'session'})
      const session = JSON.parse(sessionStorage.value)

      this.choices.push(session.game, session.mode)

      resolve(this.choices)
    })
  }


  async saveAndNextStep(goal: Goal): Promise<void> {
    // Get previous Session in localStorage
    const sessionStorage = await Storage.get({key: 'session'})
    const session = JSON.parse(sessionStorage.value)

    // Get selected Game in localStorage (necessary for redirection)
    const selectedGameStorage = await Storage.get({key: 'tmpGame'})
    const selectedGame = JSON.parse(selectedGameStorage.value)

    // Values already set
    const gamename = session.game
    const gameId = session.gameId
    const mode = session.mode

    // Check the platform if there's only one choice, this choice will be applied otherwise "null" is set
    let platform = null

    if (selectedGame.game.platforms.length === 1) {
      platform = selectedGame.game.platforms[0]
    }

    // Set the new data
    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: gamename,
        gameId,
        mode,
        goal: goal.name,
        platform,
        date: null,
        description: null,
        createdAt: null,
        expiredAt: null,
        createdBy: null,
        listPlayers: []
      })
    })



    if (selectedGame.game.platforms.length === 1) {
      await this.router.navigateByUrl('tabs/session/date')
    } else {
      await this.router.navigateByUrl('tabs/session/platform')
    }
  }

  async cancelSession(): Promise<void> {
    await this.router.navigateByUrl('tabs/homepage')
  }
}
