import { Component, OnInit } from '@angular/core'
import { Plugins } from '@capacitor/core'
import { Gamemode } from '../../../interfaces/Gamemode'
import { Router } from '@angular/router'
import { Game } from '../../../interfaces/Game'
import { LoadingController } from '@ionic/angular'
const  { Storage } = Plugins


@Component({
  selector: 'app-session-mode',
  templateUrl: './session-mode.page.html',
  styleUrls: ['./session-mode.page.scss'],
})

export class SessionModePage implements OnInit {
  game: Game = null
  choices: any[] = []
  gamemodes: Gamemode[] = null

  constructor(
      private router: Router,
      private loadingController: LoadingController,
  ) { }

  ngOnInit() {
  }

   async ionViewWillEnter() {
    const loading = await this.loadingController.create()
    await loading.present()

    await Promise.all<Promise<any>>([this.getModesFromStorage(), this.getSessionFromStorage()])
        .then(() => {
          loading.dismiss()
        })
        .catch(err => console.log(err))
  }


  /**
   * Get the data from localStorage and set global variables
   */
  private getModesFromStorage: () => Promise<Gamemode[]> = async () => {
    return new Promise (async resolve => {
      // Retrieve data from localStorage
      const tmpGameStorage = await Storage.get({key: 'tmpGame'})
      const tmpGameJson = JSON.parse(tmpGameStorage.value)

      // Set values to access from Frontend
      this.game = tmpGameJson.game
      this.gamemodes = tmpGameJson.game.gamemodes

      resolve(this.gamemodes)
    })
  }

  private getSessionFromStorage: () => Promise<any[]> = async ()  => {
    return new Promise (async resolve => {
      const sessionStorage = await Storage.get({key: 'session'})
      const session = JSON.parse(sessionStorage.value)
      this.choices.push(session.game)

      resolve(this.choices)
    })
  }

  async saveAndNextStep(mode: Gamemode) {
    // Constructing the session to be send at the end of the process
    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: this.game.name,
        gameId: this.game.id,
        mode: mode.name,
        goal: null,
        platform: null,
        date: null,
        description: null,
        createdAt: null,
        expiredAt: null,
        createdBy: null,
        listPlayers: []
      })
    })

    // const sessionStorage = await Storage.get({key: 'session'})
    // const sessionJson = JSON.parse(sessionStorage.value)
    // console.log(sessionJson)

    await this.router.navigateByUrl('tabs/session/goal')

  }

}
