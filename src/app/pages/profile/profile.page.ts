import { Component, OnInit } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { Router } from '@angular/router'
import { AuthService } from '../../services/auth/auth.service'
import { AlertController } from '@ionic/angular'
import { ScreensizeService } from '../../services/screensize/screensize.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})

export class ProfilePage implements OnInit {

  userEmail: string = null
  isDesktop: boolean

  constructor(
      private afAuth: AngularFireAuth,
      private auth: AuthService,
      private router: Router,
      private alertController: AlertController,
      private screensizeService: ScreensizeService
  ) {
    this.screensizeService.isDesktopView().subscribe(isDesktop => {
      if (this.isDesktop && !this.isDesktop){
        window.location.reload()
      }
      this.isDesktop = isDesktop
    })
  }


  ngOnInit() {

  }

  async ionViewWillEnter() {
    await this.getUserEmail()
  }


  async signOut() {
    await this.afAuth.signOut()
    await this.router.navigate(['/'])
  }

  async getUserEmail() {

    // Get current user (uid)
    await this.auth
        .getCurrentUser().then((res) => {
              this.userEmail = res.email
              // console.log(this.userEmail);
            },
            async (err) => {
              console.log('Error: ' + err)
              const alert = await this.alertController.create({
                header: 'Erreur lors de l\'i inscription',
                message: err.message,
                buttons: ['OK'],
              })
              await alert.present()
            })
  }

  /**
   * Action for deleting the current user account.
   * It will show an alert message. The user must click on confirm to delete his account
   */
  async deleteAccount() {

    const alert = await this.alertController.create({
      cssClass: 'alert-delete-account',
      header: 'Suppression compte',
      message: '<strong>Tu es sur le point de supprimer ton compte. <br /> Cette action est irrémédiable et toutes tes données seront effacées</strong> !',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Annuler' + blah)
          }
        }, {
          text: 'J\'ai compris',
          handler: () => {
            this.auth
                .deleteCurrentUser().then(() => {
                      console.log('Compte supprimé')
                    },
                    async (err) => {
                      console.log('Error: ' + err)
                      // tslint:disable-next-line:no-shadowed-variable
                      const alert = await this.alertController.create({
                        header: 'Erreur lors de la suppression du compte',
                        message: err.message,
                        buttons: ['OK'],
                      })

                      await alert.present()
                    })
          }
        }
      ]
    })

    await alert.present()

  }
}
