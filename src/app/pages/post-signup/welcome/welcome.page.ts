import { Component, OnInit } from '@angular/core'
import { ModalBaseComponent } from '../../../components/modal-base/modal-base.component'
import { ResetPwPage } from '../../reset-pw/reset-pw.page'
import { AccountPseudosPage } from '../account-pseudos/account-pseudos.page'
import { AlertController, IonRouterOutlet, LoadingController, ModalController } from '@ionic/angular'
import { FormBuilder, FormGroup } from '@angular/forms'
import { AuthService } from '../../../services/auth/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-account-infos',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})

export class WelcomePage implements OnInit {


  constructor(
      private modalCtrl: ModalController,
      private routerOutlet: IonRouterOutlet,
  ) {}

  ngOnInit() {
  }

  async openNextPage() {
    const modal = await this.modalCtrl.create({
      component: ModalBaseComponent,
      presentingElement: this.routerOutlet.nativeEl,
      swipeToClose: false,
      componentProps: {
        rootPage: AccountPseudosPage,
      },
    })
    await modal.present()
  }

}
