import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { IonicModule } from '@ionic/angular'

import { AccountPseudosPageRoutingModule } from './account-pseudos-routing.module'

import { AccountPseudosPage } from './account-pseudos.page'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        AccountPseudosPageRoutingModule
    ],
  declarations: [AccountPseudosPage]
})

export class AccountPseudosPageModule {}
