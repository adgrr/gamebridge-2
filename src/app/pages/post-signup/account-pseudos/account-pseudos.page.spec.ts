import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { AccountPseudosPage } from './account-pseudos.page'

describe('AccountPseudosPage', () => {
  let component: AccountPseudosPage
  let fixture: ComponentFixture<AccountPseudosPage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPseudosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(AccountPseudosPage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
