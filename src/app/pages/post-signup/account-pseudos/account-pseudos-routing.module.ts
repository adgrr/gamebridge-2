import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AccountPseudosPage } from './account-pseudos.page'

const routes: Routes = [
  {
    path: '',
    component: AccountPseudosPage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountPseudosPageRoutingModule {}
