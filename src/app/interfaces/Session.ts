/**
 * uid = Unique id
 * listPLayers: UID of the players that joined the session
 */
export interface Session{
    uid: string | null,
    platform: string | null,
    game: string | null,
    gameId: string | null,
    mode: string | null,
    goal: string | null,
    date: number | null,
    description: string | null,
    createdAt: string | null,
    expiredAt: string | null,
    createdBy: string | null,
    listPlayers: string[] | null,
}
