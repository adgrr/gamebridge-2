import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  /**
   * Used for the searchbar
   *
   * @description This will return an array with the values being included in the for loop
   * @param arreglo: any[]
   *
   * @description Searchbar input where the user will insert his value
   * @param text: string
   *
   * @description A column represents word separated by spacing. Generic value.
   * @param column: string
   */

  transform(arreglo: any[], text: string, column: string): any[] {
    if (text === ''){
      return arreglo
    }

    return arreglo.filter(item => {
      return item[column].toLowerCase().includes(text.toLocaleLowerCase())
    })
  }

}
